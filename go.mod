module gitlab.com/mskarbe/node-label-controller

go 1.13

require (
	github.com/go-logr/logr v0.1.0
	k8s.io/api v0.17.1
	k8s.io/apimachinery v0.17.1
	k8s.io/client-go v0.0.0-20191114101535-6c5935290e33
	sigs.k8s.io/controller-runtime v0.4.0
)
